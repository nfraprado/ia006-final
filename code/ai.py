import numpy as np
from scipy.spatial import distance
from mlp import MLP
from board import Board


class AI():

    def __init__(self, coefs=None, intercepts=None):
        if coefs is None or intercepts is None:
            coefs, intercepts = self.gen_values([MLP.N_INPUTS, 10, MLP.N_OUTPUTS])
        self.mlp = MLP(coefs, intercepts, 'tanh')
        self.board = Board()
        self.done = False


    def gen_values(self, sizes):
        coefs = []
        intercepts = []
        for i in range(len(sizes)-1):
            coefs.append(np.random.uniform(-MLP.tanh_bound_val(sizes[i],
                sizes[i+1]), MLP.tanh_bound_val(sizes[i], sizes[i+1]),
                sizes[i]*sizes[i+1]).reshape(sizes[i+1], sizes[i]))
            intercepts.append(np.random.uniform(-MLP.tanh_bound_val(sizes[i],
                sizes[i+1]), MLP.tanh_bound_val(sizes[i], sizes[i+1]),
                sizes[i+1]).reshape(sizes[i+1], 1))

        return coefs, intercepts


    def get_inputs(self):
        inputs = []
        for direc in ['straight', 'left', 'right']:
            pos = self.board.snake.body[0]
            next_pos = self.board.snake.get_next_pos(direc)
            inputs.append(0 if self.board.snake.collides(direc) or not
                          self.board.check_bounded(next_pos) else 1)
            inputs.append(1 if distance.cityblock(next_pos, self.board.food) <
                    distance.cityblock(pos, self.board.food) else 0)

        return inputs


    def get_move(self):
        inputs = np.c_[self.get_inputs()]
        outputs = self.mlp.predict(inputs)
        # get direction with greatest value
        direc = ['straight', 'left', 'right'][list(outputs).index(max(outputs))]

        return direc


    def make_move(self):
        direc = self.get_move()

        if not self.board.make_move(direc):
            self.done = True

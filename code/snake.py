class Snake():
    INITIAL_SIZE = 3
    DIRS = [[0, -1], [1, 0], [0, 1], [-1, 0]] # ^ > v <

    def __init__(self, posx, posy):
        self.body = []
        for i in range(0, Snake.INITIAL_SIZE):
            self.body.append([posx-i, posy])


    def get_dir(self):
        dir_vec = [self.body[0][0] - self.body[1][0],
                   self.body[0][1] - self.body[1][1]]

        return dir_vec


    def collides(self, direc):
        next_pos = self.get_next_pos(direc)
        if next_pos in self.body:
            return True
        else:
            return False


    def get_next_pos(self, direc):
        dir_vec = self.get_dir()
        if direc == "left":
            dir_vec = Snake.DIRS[(Snake.DIRS.index(dir_vec) - 1) % len(Snake.DIRS)]
        if direc == "right":
            dir_vec = Snake.DIRS[(Snake.DIRS.index(dir_vec) + 1) % len(Snake.DIRS)]

        return [self.body[0][0] + dir_vec[0], self.body[0][1] + dir_vec[1]]


    def move(self, direc, food):
        next_pos = self.get_next_pos(direc)

        self.body.insert(0, next_pos)

        if not food:
            self.body.pop()

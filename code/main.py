#!/usr/bin/env python3
import pygame
import sys
import matplotlib.pyplot as plt
from datetime import datetime as time
from ga import GA
from board import Board

class Game():

    pixel_size = (5, 5)
    snake_color = (255, 255, 255)
    food_color = (255, 0, 0)
    dead_color = (0, 0, 0)
    size_pop = 48
    boards_per_row = 8
    paused = False
    fps = 30

    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((Game.boards_per_row *
            (Board.sizew + 3) * Game.pixel_size[0],
            int(Game.size_pop / Game.boards_per_row) * (Board.sizeh + 3) *
            Game.pixel_size[1]))
        self.clock = pygame.time.Clock()
        self.ga = GA(Game.size_pop)


    def draw_pixel(self, color, xy):
        pygame.draw.rect(self.screen, color, (xy[0] * self.pixel_size[0],
                         xy[1] * self.pixel_size[1], self.pixel_size[0], self.pixel_size[1]))


    def draw_boards(self, ga):
        for n, indv in enumerate(ga.pop):
            board = indv.board
            offw = (n % Game.boards_per_row) * (board.sizew + 3)
            offh = int(n / Game.boards_per_row) * (board.sizeh + 3)

            max_fit = ga.get_rank()[0][1]
            if max_fit < 1:
                max_fit = 1
            alpha = max(max(0, ga.pop[indv])/max_fit, 0.1)
            snake_color_norm = (self.snake_color[0]*alpha, self.snake_color[1]*alpha,
                                self.snake_color[2]*alpha)
            # Whiteness proportional to fitness in snake body color

            for snake_sq in board.snake.body:
                self.draw_pixel(snake_color_norm, (snake_sq[0] + offw,
                    snake_sq[1] + offh))

            self.draw_pixel(self.food_color, (board.food[0] + offw,
                board.food[1] + offh))

            # Black colored wall when dead
            wall_color = self.snake_color if not indv.done else self.dead_color

            for i in range(0, board.sizew+1):
                self.draw_pixel(wall_color, (0 + offw, i + offh))
            for i in range(0, board.sizew+1):
                self.draw_pixel(wall_color, (board.sizeh+1 + offw, i + offh))
            for i in range(0, board.sizeh+1):
                self.draw_pixel(wall_color, (i + offw, 0 + offh))
            for i in range(0, board.sizeh+2):
                self.draw_pixel(wall_color, (i + offw, board.sizew+1 + offh))


    def export_mlp_coefs(self):
        now = time.now()
        with open(f'../out/mlp_{now.hour}:{now.minute}:{now.second}.txt', 'w') as f:
            for n, indv in enumerate(self.ga.pop.keys()):
                f.write(f'N: {n}\n')
                f.write(f'C: {indv.mlp.coefs}\n')
                f.write(f'I: {indv.mlp.intercepts}\n')

    def plot_fit_hist(self):
        now = time.now()
        avgs = []
        for gen, fits in enumerate(self.ga.fit_hist):
            avgs.append(sum(fits) / len(fits))
            plt.scatter([gen] * len(fits), fits, color='b')

        gens = list(range(len(self.ga.fit_hist)))
        plt.plot(gens, avgs, color='r')
        plt.xlabel('Geração')
        plt.ylabel('Fitness')
        plt.grid()
        plt.savefig(f'../fig/plot_fitness_{now.hour}:{now.minute}:{now.second}.png')
        plt.clf()


    def handle_key_down(self, event):
        if event.key == pygame.K_ESCAPE or event.key == pygame.K_q:
            pygame.quit()
            sys.exit()
        elif event.key == pygame.K_SPACE or event.key == pygame.K_p:
            self.paused = not self.paused
        elif self.paused and (event.key == pygame.K_PERIOD or event.key ==
                pygame.K_s):
            self.tick()
        elif event.key == pygame.K_g:
            self.plot_fit_hist()
        elif event.key == pygame.K_n:
            self.export_mlp_coefs()
        elif event.key == pygame.K_EQUALS:
            if self.fps < 60:
                self.fps += 5
        elif event.key == pygame.K_MINUS:
            if self.fps > 5:
                self.fps -= 5


    def tick(self):
        self.screen.fill((0, 0, 0))
        self.ga.iterate()
        self.draw_boards(self.ga)
        pygame.display.update()


    def run(self):
        while True:
            if not self.paused:
                self.tick()

            msElapsed = self.clock.tick(self.fps)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    self.handle_key_down(event)


g = Game()
g.run()

import random
from snake import Snake

class Board():

    sizew = 30
    sizeh = 30

    def __init__(self):
        self.snake = Snake(Board.sizew/2, Board.sizeh/2)
        self.new_food()


    def new_food(self):
        while True:
            self.food = [random.randint(1, self.sizew),
                         random.randint(1, self.sizeh)]
            if self.food not in self.snake.body:
                break


    def check_bounded(self, coord):
        if (coord[0] >= 1 and coord[0] <= self.sizew and coord[1] >= 1 and
            coord[1] <= self.sizeh):
            return True
        else:
            return False


    def make_move(self, direc):
        next_pos = self.snake.get_next_pos(direc)

        if self.snake.collides(direc) or not self.check_bounded(next_pos):
            return False

        if next_pos == self.food:
            self.new_food()
            self.snake.move(direc, 1)
        else:
            self.snake.move(direc, 0)

        return True

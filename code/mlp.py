from sklearn.neural_network._base import ACTIVATIONS
import numpy as np
import sys


class MLP():
    HIDDEN_LAYER_SIZES = (10)
    N_INPUTS = 6
    N_OUTPUTS = 3

    def tanh_bound_val(fan_in, fan_out):
        return np.sqrt(6. / (fan_in + fan_out))


    def __init__(self, coefs, intercepts, activation):
        if len(coefs[0][0]) != MLP.N_INPUTS or len(coefs[-1]) != MLP.N_OUTPUTS:
            print("Invalid coefs matrix size")
            sys.exit(1)
        self.coefs = coefs
        self.intercepts = intercepts
        self.activation = ACTIVATIONS[activation]


    def predict(self, X):
        out = X
        for i in range(0, len(self.coefs)):
            out = self.activation(np.matmul(self.coefs[i], out) +
                    self.intercepts[i])

        return out

from scipy.spatial import distance
import numpy as np
from ai import AI
from mlp import MLP

class GA():
    MIN_FIT = -180

    def __init__(self, n_pop):
        self.fit_hist = []
        self.pop = {}
        self.n_pop = n_pop
        for indv in range(n_pop):
            self.add_indv(AI())


    def get_rank(self):
        return sorted(self.pop.items(), key=lambda x:x[1], reverse=True)


    def save_fit(self):
        self.fit_hist.append(list(self.pop.values()))


    def new_round(self):
        self.save_fit()

        print(f"Max fitness: {max(ai[1] for ai in self.get_rank())}")
        print(f"Mean fitness: {sum(ai[1] for ai in self.get_rank())/self.n_pop}")
        print(f"Min fitness: {min(ai[1] for ai in self.get_rank())}")
        winner_pop = self.roullete()
        self.pop = {}
        for parents in winner_pop:
            child = self.make_new_indv(parents[0], parents[1])
            self.add_indv(child)

        # Add remaining random individuals
        for i in range(self.n_pop - len(self.pop)):
            self.add_indv(AI())


    def roullete(self):
        winners = []
        rank = self.get_rank()

        if all(indv[1] <= 0 for indv in rank):
            for i in range(self.n_pop):
                parents = []
                for parent in range(2):
                    parents.append(rank[np.random.randint(len(rank))][0])
                winners.append(parents)
            return winners

        total_fit = sum([max(indv[1], 0) for indv in rank])
        for i in range(self.n_pop):
            parents = []
            for parent in range(2):
                ran = np.random.randint(int(total_fit))
                cur_fit = 0
                for indv in rank:
                    cur_fit += max(indv[1], 0)
                    if cur_fit > ran:
                        parents.append(indv[0])
                        break
            winners.append(parents)

        return winners


    def add_indv(self, indv):
        self.pop[indv] = 0


    def make_new_indv(self, par1, par2):
        return self.mutate(self.crossover(par1, par2))


    def mutate(self, indv):
        coefs = indv.mlp.coefs
        intercepts = indv.mlp.intercepts

        layers = len(coefs)

        layer = np.random.randint(layers)
        lines = len(coefs[layer])
        line = np.random.randint(lines)
        cols = len(coefs[layer][line])
        col = np.random.randint(cols)

        coefs[layer][line][col] += (np.random.uniform(-MLP.tanh_bound_val(cols,
            lines)/50, MLP.tanh_bound_val(cols, lines)/50))
        intercepts[layer][line] += (np.random.uniform(-MLP.tanh_bound_val(cols,
            lines)/50, MLP.tanh_bound_val(cols, lines)/50))

        return indv


    def crossover(self, par1, par2):
        coefs = np.copy(par1.mlp.coefs)
        intercepts = np.copy(par1.mlp.intercepts)

        for layer in range(len(coefs)):
            for neuron in range(len(coefs[layer])):
                cross = np.random.randint(len(coefs[layer][0]))
                coefs[layer][neuron] = list(coefs[layer][neuron][:cross]) + list(par2.mlp.coefs[layer][neuron][cross:])
            cross = np.random.randint(len(intercepts[layer]))
            intercepts[layer] = list(intercepts[layer][:cross]) + list(par2.mlp.intercepts[layer][cross:])

        child = AI(coefs, intercepts)
        return child


    def iterate(self):
        if all([indv.done for indv in self.pop.keys()]):
            self.new_round()

        for n, indv in enumerate(self.pop):
            if not indv.done:
                indv.make_move()
                self.pop[indv] += self.delta_fitness(indv)
                if self.pop[indv] < GA.MIN_FIT:
                    indv.done = True


    def delta_fitness(self, indv):
        
        snake = indv.board.snake
        old_pos = snake.body[1]
        new_pos = snake.body[0]
        old_dist = distance.cityblock(old_pos, indv.board.food)
        new_dist = distance.cityblock(new_pos, indv.board.food)
        if new_dist == 0:
            delta_fitness = 20
        
        else:
            delta_fitness = 2 if new_dist < old_dist else -3
        
        return delta_fitness

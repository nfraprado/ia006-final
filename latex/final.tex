\documentclass[11pt]{article}
\usepackage{final-template}
\usepackage[plain]{algorithm}

\usepackage[brazil]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{graphicx,url}
\usepackage[hang]{subfigure}

\sloppy

\expandafter\def\expandafter\UrlBreaks\expandafter{\UrlBreaks\do-}

\title{Treinamento de MLP utilizando algoritmo evolutivo para jogar \emph{Snake}}

\author{Laura R. R. Pereira\and Lucas Guimarães Braga \and Nícolas F. R. A. Prado}
\address{\email{\{l178193@dac.unicamp.br,l182543@dac.unicamp.br,n185142@dac.unicamp.br\}}}

\begin{document}

\hyphenation{}
\pagestyle{fancy}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\twocolumn[
\maketitle
\thispagestyle{fancy}

   \begin{abstract}
       Algoritmos evolutivos podem ser utilizados para treinamento sem necessidade de fornecimento de uma base de dados.
       Utilizando-se esse tipo de algoritmo, o treinamento de MLPs para jogar \emph{Snake} pode ser feito em tempo real, fornecendo-se o estado atual do jogo e obtendo-se da saída da rede o melhor movimento disponível.
       Utilizando-se apenas seis simples entradas na rede e uma única camada intermediária, é possível obter do treinamento certo nível de aprendizado, ainda que os resultados se mostrem distantes de um algoritmo capaz de vencer o jogo.
   \end{abstract}

  \keywords{inteligência artificial, algoritmo evolutivo, algoritmo genético, snake}

]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  \section{Introdução}
  \label{sec:introducao}
  Aprendizado de máquina têm sido utilizado amplamente para resolução de problemas.
  Um paradigma muito utilizado é o de aprendizado supervisionado.
  No entanto, há casos em que este é impraticável, principalmente quando não há dados de treinamento a serem fornecidos.
  Para esses casos pode-se utilizar métodos não supervisionados.

  Dentre diversos métodos de aprendizado de máquina não supervisionado, os algoritmos evolutivos se mostram interessantes heurísticas que, apesar de não garantirem convergência, tendem a ser simples de utilizar.


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \section{Proposta}
  \label{sec:detalhes}

    Utilizar algoritmo evolutivo para obter uma rede MLP que tenha bom desempenho no jogo \emph{Snake}.
    A estrutura do algoritmo aqui discutida foi baseada em \cite{main}.

    A população utilizada pelo algoritmo é formada por 48 indivíduos, cada um deles composto por um tabuleiro, uma cobra e uma MLP.
    As matrizes de pesos dos neurônios e os vetores de \emph{bias} de cada camada compõem o genoma do indivíduo.

    A cobra tem tamanho inicial 3 e o tabuleiro tem dimensões de 30x30, excluindo as paredes.

    A cada geração, a MLP de cada um dos indivíduos recebe suas entradas com base em sua cobra e tabuleiro e fornece, em sua saída, o próximo movimento a ser realizado.
    Esse processo é repetido até que a geração se encerre.

    A geração termina quando todos os indivíduos estão mortos.
    Quando um indivíduo atinge o limite mínimo de \emph{fitness}, ele é morto.
    Esse limite mínimo é inserido para evitar que uma geração nunca acabe caso apresente indivíduos que se movimentam em \emph{loop}.
    O limite mínimo estabelecido foi de -180, dado que é o valor que seria obtido por uma cobra que se movimenta de uma ponta à outra do tabuleiro, em direção oposta à comida, e portanto não é um cenário esperado de um indivíduo adaptado.

    A seleção dos indivíduos é feita pelo algoritmo da roleta, com a ressalva de que aqueles que possuem \emph{fitness} negativo não têm probabilidade de serem selecionados.
    Caso todos os \emph{fitness} sejam negativos, os indivíduos são escolhidos aleatoriamente.

    O número de indivíduos selecionados é o dobro do tamanho da população.
    Para cada par é realizado \emph{crossover}, gerando um indivíduo descendente.
    Este sofre uma mutação aleatória em um de seus genes e é adicionado à população da próxima geração.
    É possível que o par de indivíduos sorteados seja o mesmo.
    Nesse caso, o descendente será o próprio pai apenas modificado por uma mutação.

    A rede MLP de cada indivíduo possui 6 entradas, 3 saídas e uma única camada intermediária de tamanho 10, como mostrado na figura~\ref{fig:rede}.
    Suas entradas são a presença ou não de obstáculo --- parede ou corpo da cobra --- em cada uma das três direções de movimento (direita, esquerda e em frente); e se a comida está ou não para cada uma das três direções.
    As saídas são cada uma das três direções de movimento possíveis para a cobra.
    Aquela que tiver maior valor será a escolhida para ser realizada.

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{fig/neural_network.png}
    \caption{Representação da rede neural implementada}
    \label{fig:rede}
\end{figure}

    O \emph{fitness} do indivíduo decrementa 3 toda vez que a cobra se afasta da comida e incrementa 2 toda vez que se aproxima.
    Essa assimetria tem como objetivo punir indivíduos que entrem em \emph{loop}, já que a esperança de seu \emph{fitness} será negativa e portanto será descartado pelo algoritmo genético.
    Além disso, o \emph{fitness} aumenta em 20 toda vez que uma comida é alcançada, assim há uma maior pressão para de fato se obter a comida e não apenas se aproximar dela.

    A inicialização dos pesos das sinapses da MLP é feita de maneira aleatória utilizando uma distribuição uniforme, respeitando os limites utilizados pela biblioteca \emph{scikit-learn}, que por sua vez se baseia em \cite{glorot}.
    A expressão é dada por

    \begin{equation}\label{eq:init}
        W \sim{} U\left[ -\frac{\sqrt{6}}{\sqrt{n_j+n_{j+1}}},
        \frac{\sqrt{6}}{\sqrt{n_j+n_{j+1}}}\right],
    \end{equation} onde $n_j$ e $n_{j+1}$ representam respectivamente o número de neurônios da camada anterior e da camada seguinte.

    Para cada novo descente, é aplicada uma única mutação em um dos pesos da MLP e outra em um dos \emph{bias}, ambos escolhidos aleatoriamente.
    O valor da mutação segue a mesma distribuição da equação~\ref{eq:init} porém com $\frac{1}{50}$ do intervalo.
    Essa diminuição do intervalo tem por fim suavizar a mudança, já que esse valor é adicionado ao peso que sofre a mutação.
\section{Resultados}

Em busca de atingir os objetivos já detalhados, foi implementado um programa em \emph{python}.
A implementação da parte gráfica e da interatividade do programa utilizaram a biblioteca \emph{pygame} \cite{pygame}.

A figura~\ref{fig:tela} mostra a tela do programa.
Os indivíduos cujo contorno do tabuleiro está apagado estão mortos.
A cor da cobra de cada indivíduo depende da relação entre seu \emph{fitness} e o \emph{fitness} máximo: é totalmente branca para aquele que tiver maior \emph{fitness} e mais escura quanto menor for seu \emph{fitness} em relação ao maior da geração atual.

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{fig/tela.png}
    \caption{Tela do programa}
    \label{fig:tela}
\end{figure}

A figura~\ref{fig:fit} mostra a evolução durante uma das execuções do programa.
Cada um dos pontos representa o \emph{fitness} de um indivíduo.
A reta vermelha representa a evolução média do \emph{fitness} dos indivíduos de cada geração.

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{fig/evol_fitness.png}
    \caption{Evolução do desempenho dos indivíduos ao longo das gerações}
    \label{fig:fit}
\end{figure}

Como pode ser observado pela figura~\ref{fig:fit}, há uma evolução da média do \emph{fitness}, principalmente nas primeiras gerações.
O valor médio, no entanto, se torna constante após algumas gerações.
Mesmo havendo indivíduos com \emph{fitness} muito maior que a média, há inclusive gerações subsequentes em que os indivíduos demonstram \emph{fitness} muito menor.
A não monotonicidade no crescimento, podendo inclusive possuir decrescimentos, do \emph{fitness} é uma característica de heurísticas, como algoritmos evolutivos: não há garantias de convergência.

A figura~\ref{fig:fit2} mostra novamente a evolução do \emph{fitness}, porém para um execução diferente e, assim, para uma diferente inicialização dos dados.
A comparação entre as figuras torna clara uma característica de algoritmos evolutivos: alta dependência do estado inicial.

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{fig/evol_fitness2.png}
    \caption{Evolução do desempenho dos indivíduos ao longo das gerações para outra execução}
    \label{fig:fit2}
\end{figure}

A figura~\ref{fig:bom} mostra a população de uma geração bem adaptada.

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{fig/pop_bom.png}
    \caption{População de uma geração bem adaptada}
    \label{fig:bom}
\end{figure}

O código final do projeto está disponível em \url{https://gitlab.com/nfraprado/ia006-final}.

\section{Conclusões}

Ainda que tenha sido possível observar pressão do algoritmo no sentido da evolução dos indivíduos, ela não se mostrou suficiente para que atingissem um resultado satisfatório, visto que os indivíduos gerados ainda estão distantes do vencer o jogo.

Como trabalhos futuros vislumbra-se a possibilidade de se experimentar com diferentes operadores genéticos e de seleção, a fim de se obter uma população com maior diversidade e que assim seja menos sujeita à parada em máximos locais.

Outra possibilidade de melhoria do desempenho é utilizar um modelo de rede mais elaborado.
Como o modelo utilizado fornece apenas a presença de obstáculos nas três posições vizinhas à cobra, não há como este modelo evitar situações em que a cobra entra em uma cavidade, formada por seu corpo, menor que o comprimento de seu corpo e da qual portanto não é possível sair viva.

Finalmente, há também a inadequação da função fitness ao final do jogo.
Com o avanço do jogo, a cobra tem seu comprimento aumentado e assim necessita percorrer caminhos mais longos para chegar à comida sem colidir consigo mesma.
Assim, a punição gerada para um deslocamento no sentido oposto ao da comida, que é necessário nessa etapa do jogo, causa uma grande diminuição do \emph{fitness} do indivíduo, e assim este perde preferência no algoritmo, dando espaço para indivíduos que seguem o menor caminho em direção à comida e que portanto são mais eficientes no início do jogo porém inadequados ao fim do jogo.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \bibliographystyle{plain}

   \bibliography{bib-final}

\end{document}

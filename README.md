# Projeto final IA006

Projeto final da disciplina de Aprendizado de Máquina (IA006), FEEC, Unicamp.

Este projeto implementa o treinamento de MLPs utilizando algoritmo evolutivo
para jogar *Snake*.

O diretório `latex/` contém o relatório final do projeto.
O diretório `code/` contém todo o código do programa.

## Dependências

* Numpy
* Scipy
* Pygame

## Estrutura

- [x] Snake
  - [x] Lógica/regras
  - [x] Componentes
    - [x] Cobra
    - [x] Tabuleiro
  - [x] Pygame
    - [x] Setup
    - [x] Desenho
- [x] AI
  - [x] Rede neural
    - [x] Estrutura: Camadas, tamanhos, entradas, saídas
  - [x] Algoritmo genético
    - [x] Seleção (fitness)
    - [x] Operadores genéticos

## Utilização

### Teclas

* `Esc`/`Q`: Fecha a simulação.
* `Space`/`P`: Pausa a simulação.
* `.`/`S`: Avança um passo da simulação. A simulação deve estar pausada.
* `G`: Gera o gráfico de evolução de fitness dos indivíduos.
* `N`: Exporta os coeficientes e valores de bias das MLPs dos indivíduos da
  geração atual.
* `=`: Aumenta FPS em 5. Máximo é 60.
* `-`: Diminui FPS em 5. Mínimo é 5.

## Referências
* https://becominghuman.ai/designing-ai-solving-snake-with-evolution-f3dd6a9da867
* https://sites.cs.ucsb.edu/~pconrad/cs5nm/topics/pygame/drawing/
* https://github.com/scikit-learn/scikit-learn/blob/f3320a6f/sklearn/neural_network/multilayer_perceptron.py#L300-L316
